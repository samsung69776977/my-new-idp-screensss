require("dotenv").config();
const express = require("express");
const fs = require("fs");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");
const mongoose = require("mongoose");
const multer = require("multer");
const upload = multer();
const port = process.env.PORT || 3001;
const corsOptions = {
  methods: ["GET", "POST", "OPTIONS", "PUT", "DELETE"],
  allowedHeaders: ["Content-Type"],
};
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(cors());
app.use(bodyParser.json());
app.use(express.json());
mongoose
  .connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("MongoDB connected successfully!"))
  .catch((err) => console.error("Mongo connection error:", err));
// const userSchema = new mongoose.Schema({
//   username: { type: String, required: true, unique: true },
//   password: { type: String, required: true }
// });
const userSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  email: { type: String, required: false },
  password: { type: String, required: true },
  phoneNumber: { type: String, required: false },
  agreeToTerms: { type: Boolean, required: false }
});
const SECRET_KEY = "#$Q^TGTHYHBFY";
const DataSchema = new mongoose.Schema(
  {},
  {
    strict: false,
    timestamps: true,
  }
);
const DataModel = mongoose.model("Data", DataSchema);
const User = mongoose.model('User', userSchema);
function renameKeys(document, keyMappings) {
  const newDocument = {};
  Object.keys(document).forEach((key) => {
    const newKey = keyMappings[key] || key;
    newDocument[newKey] = document[key];
  });
  return newDocument;
}
let divVisible = true;
app.post('/api/create-users', async (req, res) => {
  try {
    const { username, email, password, phoneNumber, agreeToTerms } = req.body;
    const newUser = new User({
      username,
      email,
      password,
      phoneNumber,
      agreeToTerms
    });
    const savedUser = await newUser.save();
    res.status(201).json(savedUser);
  } catch (err) {
    console.error('Error creating user:', err);
    res.status(500).json({ error: 'Failed to create user' });
  }
});
app.post('/api/check-user', async (req, res) => {
  try {
    const { username } = req.body;
    const userExists = await User.findOne({ username: username });
    if (userExists) {
      res.json({ exists: true });
    } else {
      res.json({ exists: false });
    }
  } catch (error) {
    res.status(500).send('Error checking username.');
  }
});
// username":"default@sirmaindia.com","password":"masterpassword@123"
app.post('/login', async (req, res) => {
  const { username, password } = req.body;
  try {
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(401).json({ message: "Authentication failed: User not found." });
    }
    if (password !== user.password) {
      return res.status(401).json({ message: "Authentication failed: Incorrect password." });
    }
    res.json({ message: "Login successful!", userId: user.id });
  } catch (error) {
    console.error("Error during login:", error);
    res.status(500).json({ message: "An error occurred during login" });
  }
});
app.post('/change-password', async (req, res) => {
  const { username, newPassword, secretKey } = req.body;
  if (!username || !newPassword || !secretKey) {
    return res.status(400).json({ message: "Missing fields: username, new password or secret key." });
  }
  if (secretKey !== SECRET_KEY) {
    return res.status(401).json({ message: "Unauthorized: Invalid secret key." });
  }
  try {
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(404).json({ message: "User not found." });
    }
    user.password = newPassword;
    await user.save();
    res.json({ message: "Password successfully updated!" });
  } catch (error) {
    console.error("Error during password change:", error);
    res.status(500).json({ message: "An error occurred during password change." });
  }
});
app.post("/api/receiveData", async (req, res) => {
  console.log("Received a POST request to /api/receiveData");
  console.log("Request body:", req.body);
  try {
    console.log(
      "Attempting to create a new document with the received data..."
    );
    const textResult = req.body.data["i-body"].TextResult;
    console.log("Extracted TextResult:", textResult);
    const newData = new DataModel({ TextResult: textResult });
    console.log("New document created:", newData);
    console.log("Now saving to MongoDB...");
    await newData.save();
    console.log("Document saved successfully to MongoDB:", newData);
    res.status(201).send({ message: "Data saved to MongoDB", data: newData });
  } catch (error) {
    console.error("Error occurred while saving data to MongoDB:", error);
    console.log("Error details:", error.message);
    res
      .status(500)
      .send({ message: "Failed to save data", error: error.message });
  }
});
app.get("/api/fetchingData", (req, res) => {
  divVisible = !divVisible;
  res.json({ nextdatafindid: divVisible });
});
app.put("/api/updateData/:id", async (req, res) => {
  const documentId = req.params.id.trim();
  const keyMappings = req.body.keyMappings;
  try {
    const document = await DataModel.findById(documentId);
    if (!document) {
      return res.status(404).send({ message: "Document not found" });
    }
    const updatedDocumentData = renameKeys(document.toJSON(), keyMappings);
    const updatedDocument = await DataModel.findByIdAndUpdate(
      documentId,
      { $set: updatedDocumentData },
      { new: true }
    );
    if (!updatedDocument) {
      return res
        .status(404)
        .send({ message: "No document found with the given ID" });
    }
    res.status(200).send({
      message: "Document updated successfully",
      data: updatedDocument,
    });
  } catch (error) {
    console.error("Error during the update process:", error);
    res
      .status(500)
      .send({ message: "Failed to update data", error: error.message });
  }
});
app.get("/api/allData", async (req, res) => {
  try {
    const allData = await DataModel.find({});
    console.log("All data fetched from MongoDB:", allData);
    res.status(200).send(allData);
  } catch (error) {
    console.error(
      "Error occurred while fetching all data from MongoDB:",
      error
    );
    console.log("Error details:", error.message);
    res
      .status(500)
      .send({ message: "Failed to fetch all data", error: error.message });
  }
});
app.delete("/api/deleteAllData", async (req, res) => {
  try {
    const deleteResult = await DataModel.deleteMany({});
    console.log("All data deleted from MongoDB", deleteResult);
    res.status(200).send({
      message: "All data successfully deleted",
      details: deleteResult,
    });
  } catch (error) {
    console.error(
      "Error occurred while deleting all data from MongoDB:",
      error
    );
    res
      .status(500)
      .send({ message: "Failed to delete all data", error: error.message });
  }
});
app.post("/api/upload", (req, res) => {
  const data = req.body;
  fs.writeFile(
    "new-data.json",
    JSON.stringify(data, null, 2),
    "utf8",
    (err) => {
      if (err) {
        console.error(err);
        return res.status(500).send({ message: "Error writing file" });
      }
      res.send({ message: "Data successfully written to new-data.json" });
    }
  );
});
app.post("/api/coordinates", (req, res) => {
  const { x, y } = req.body;
  console.log(`Received coordinates: X=${x}, Y=${y}`);
  res.status(200).json({
    message: `Coordinates received successfully: X=${x}, Y=${y}`,
    x: x,
    y: y,
  });
});
app.get("/api/data", (req, res) => {
  fs.readFile("new-data.json", "utf8", (err, data) => {
    if (err) {
      console.error(err);
      return res.status(500).send({ message: "Error reading the file" });
    }
    res.send(data);
  });
});
app.get("/api/latestData", async (req, res) => {
  try {
    const latestData = await DataModel.findOne({}).sort({ createdAt: -1 });
    if (!latestData) {
      console.log("No data available");
      return res.status(404).send({ message: "No data found" });
    }
    console.log("Latest data fetched from MongoDB:", latestData);
    res.status(200).send(latestData);
  } catch (error) {
    console.error(
      "Error occurred while fetching the latest data from MongoDB:",
      error
    );
    console.log("Error details:", error.message);
    res.status(500).send({
      message: "Failed to fetch the latest data",
      error: error.message,
    });
  }
});
app.delete("/api/clear", (req, res) => {
  fs.writeFile("new-data.json", "[]", "utf8", (err) => {
    if (err) {
      console.error(err);
      return res.status(500).send({ message: "Error clearing the file" });
    }
    res.send({ message: "Data successfully cleared from new-data.json" });
  });
});
app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});